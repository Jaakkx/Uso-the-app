export type Utilisateur = {
    id?: string;
    email: string;
    spotifyToken:string;
    osuToken:string;
    playlistsLinks:[];
}